/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <bitset>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{

  //offset size log2(block_size)
  field_size->offset=log2(cache_params.block_size);
  //idx size = log2(sets/a)
  field_size->idx=log2((cache_params.size*KB)/(cache_params.block_size*cache_params.asociativity));
  //tag = ADDRSIZE - offset - idx
  field_size->tag=ADDRSIZE-(field_size->offset)-(field_size->idx);
  return OK;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{
  bitset<32> b(address);
  string pcstr = b.to_string();
  string tagstr,idxstr;
  tagstr=pcstr.substr(0, field_size.tag);
  idxstr=pcstr.substr((field_size.tag), field_size.idx);
  *tag=static_cast<int>(bitset<32>(tagstr).to_ulong());
  *idx=static_cast<int>(bitset<32>(idxstr).to_ulong());

}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             int rp_val,
                             bool debug)
{
   bool state = false;//se inicia en miss el resultado
	//se recorre el cache buscando si hay un hit
	for(int cache_pos = 0; cache_pos < associativity; cache_pos++){
		if(cache_blocks[cache_pos].valid == 1){
		//si el bit de valido esta en 1 se revisa este bloque, si no se sigue
			if(cache_blocks[cache_pos].tag == tag){
				state = true;
				if(loadstore == 1){
					cache_blocks[cache_pos].dirty = true;
					result -> miss_hit = HIT_STORE;
				}
				else{
					result -> miss_hit = HIT_LOAD;
				}
				//EVICTION
				result->dirty_eviction = false;
        		result->evicted_address = 0;
				//se setea el valor RRPV en 0 
				cache_blocks[cache_pos].rp_value=0;
				return OK;				
			}
		}
	}
	//miss
	if(state==false){
		int rrpv=0;
    	int bandera = 0;
    	int i = 0;
		//busca el rp mayor para remplazarlo por el nuevo tag
		while(i <= associativity && bandera == 0){
      
			if(i<associativity){
        if(cache_blocks[i].rp_value == rp_val){
				  rrpv=i;
          bandera = 1;//encontre un posible candidato a ser reemplazado
          break;
			  }
      	}
      	else{//debo actualizar los rrpv y probar nuevamente si alguno esta listo para salir
        	int j = 0;
        	while(j<associativity){
          	cache_blocks[j].rp_value = cache_blocks[j].rp_value + 1; 
          	j = j + 1;
        	}
        	i = -1;
      	}
      	i = i + 1;
		}
    //procedo a realizar el cambio
    //si se desaloja,se guardan los valores del bloque en los eviction
		result->evicted_address = cache_blocks[rrpv].tag;
		if(cache_blocks[rrpv].valid == 1){

      		if(cache_blocks[rrpv].dirty){
	      		result->dirty_eviction = true;
	    	}
      		else{
        		result->dirty_eviction = false;
      		}
    	}
		//se actualizan los valores del nuevo bloque
		cache_blocks[rrpv].tag = tag;
		cache_blocks[rrpv].valid = 1;
		//si fue store se  actualiza el dirty bit y result
		if(loadstore==1){
			cache_blocks[rrpv].dirty = true;
			result -> miss_hit = MISS_STORE;
		}
		else{
			cache_blocks[rrpv].dirty = false;
			result -> miss_hit = MISS_LOAD;
		}
		//se remplazan los valores  
		cache_blocks[rrpv].rp_value = rp_val-1;
		return OK;
	}
  if(associativity < 0 || idx < 0 || tag <  0){
    return PARAM;
  }
   return ERROR;
}


int nru_replacement_policy(int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  //Ahora el algortimo de NRU es:
  /*Cache hit:
  (i) establecer nru-bit del bloque en "0"
  Cache miss:
  (i) busque el primer "1" de la izquierda
  (ii) si se encuentra "1", vaya al paso (v)
  (iii) establezca todos los nru-bits en "1"
  (iv) ir al paso (i)
  (v) reemplace el bloque y establezca nru-bit en "1"
  */
  //inicializamos en miss
  //inicializamos en miss
  bool state = false;

  //ahora recorremos las vias para trabajarlas
  for(int cache_pos = 0; cache_pos < associativity; cache_pos++){
		//cout<<"via "<<cache_pos<<endl;
		//revisamos el bit de valido
    if(cache_blocks[cache_pos].valid == 1){ //valid 1
      //si entra revisamos tag
        if (cache_blocks[cache_pos].tag == tag){//tag esta dentro
        //si hay un hit significa que tag se encuentra en la via
            state = true; //hit
        //***********Cambiamos el rp a 0
        //***********cache_blocks[cache_pos].rp_value = 0;
        //ahora revisamos el loadstore
        //0 implica un load
        //1 implica un store
            if(loadstore == true){
          //entonces significa que se han hecho modificaciones
                cache_blocks[cache_pos].dirty = true;
                result -> miss_hit = HIT_STORE;

            }//fin loadstore true
            else{
            //no se le ha hecho ninguna modificacion
                result -> miss_hit = HIT_LOAD;
                //no se actualiza aca el dirty
			}//fin loadstore false
      //EVICTION
      result->dirty_eviction = false;
      result->evicted_address = 0;
      cache_blocks[cache_pos].rp_value = 0;
      return OK;
      }//fin tag
    }//fin valid 1
  }//fin for vias
  
  //MISS
  if(state == false){
    int nru = 0;//variable para cambiar tener la posicion del primer 1
    bool flag = false;//bandera para que salga del while
    int i = 0;//contador
    int flagv = false;
    //se recorren las vias de izquierda a derecha en busca del primer 1
    for(i; i<associativity;i++){
      if(cache_blocks[i].rp_value == 1 && flag == false){
        nru = i;
        flag = true;//true solo si se encuentra el primer 1
      }
    }
    //si todos los rp_value son 0 entonces se cambian todos los valores a 1
    if (flag == false){
      for(int j = 0; j <associativity;j++){
        cache_blocks[j].rp_value = 1;
      }
    }
    cache_blocks[nru].rp_value=0;

   
    //si se desaloja,se guardan los valores del bloque en los eviction
		
    result->evicted_address = cache_blocks[nru].tag;
    
    if(cache_blocks[nru].valid == 1){

      if(cache_blocks[nru].dirty){
	      result->dirty_eviction = true;
	    }
      else{
        result->dirty_eviction = false;
      }
      //return OK;
    }
    
		//se actualizan los valores del nuevo bloque
		cache_blocks[nru].tag = tag;
		cache_blocks[nru].valid = 1;
		//si fue store se  actualiza el dirty bit y result
		if(loadstore == 1){
			cache_blocks[nru].dirty = true;
			result -> miss_hit = MISS_STORE;
		}
		else{
			cache_blocks[nru].dirty = false;
			result -> miss_hit = MISS_LOAD;
		}
		return OK;
	}
  if(associativity < 0 || idx < 0 || tag <  0){
    return PARAM;
  }
  return ERROR;
}

int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{

	//se inicializa el resultado en miss
	bool state=false;
	//se recorre el cache buscando si hay un hit
	for(int cache_pos = 0; cache_pos < associativity; cache_pos++){
		if(cache_blocks[cache_pos].valid==1){
			//si el bit de valido esta en 1 se revisa este bloque, si no se sigue
			if(cache_blocks[cache_pos].tag==tag){
				state = true;
				//se revisa si hay un hit y luego revisa si la instruccion es store
				if(loadstore==1){
					cache_blocks[cache_pos].dirty = true;
					result -> miss_hit = HIT_STORE;
				}
				else{
					result -> miss_hit = HIT_LOAD;
				}
        //EVICTION
        result->dirty_eviction = false;
        result->evicted_address = 0;
				//se les hace update a los valores del LRU
				for(int i = 0; i < associativity; i++){
					if(cache_blocks[i].rp_value > cache_blocks[cache_pos].rp_value){
						cache_blocks[i].rp_value--;
					}
				}
				cache_blocks[cache_pos].rp_value=associativity-1;
				return OK;
			}
		}
	}
  
	//miss
	if(state==false){
		int lru=0;
        int victim = 0;
		//busca el rp menor para remplazarlo por el nuevo tag
		for(int i=0; i<associativity;i++){
			if(cache_blocks[i].rp_value < cache_blocks[lru].rp_value){
				lru=i;
			}
		}
    
    for(int v=0; v<associativity;v++){
			if(cache_blocks[v].rp_value == 0){
				victim = v;
			}
		}
		//si se desaloja,se guardan los valores del bloque en los eviction
	
		//se actualizan los valores del nuevo bloque
		cache_blocks[lru].tag = tag;
		cache_blocks[lru].valid = 1;
		
		//se remplazan los valores nuevos de LRU
		for(int i = 0; i < associativity; i++){
			if(cache_blocks[i].rp_value > cache_blocks[lru].rp_value){
				cache_blocks[i].rp_value--;
			}
		}
		cache_blocks[lru].rp_value= associativity -1;


    result->evicted_address = cache_blocks[victim].tag;
    if(cache_blocks[victim].dirty){
			result->dirty_eviction = true;
		}
    else{
      result->dirty_eviction = false;
		
    }
    //si fue store se  actualiza el dirty bit y result
		if(loadstore==1){
			cache_blocks[lru].dirty = true;
			result -> miss_hit = MISS_STORE;
		}
		else{
			cache_blocks[lru].dirty = false;
			result -> miss_hit = MISS_LOAD;
		}
		return OK;
	}
  if(associativity < 0 || idx < 0 || tag <  0){
    return PARAM;
  }
  return ERROR;
}