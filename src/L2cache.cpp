/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

//Entonces tenemos 2 niveles de cache
//el primer nivel es el cache l1 
//el segundo nivel es el cacahe l2
//ambos niveles se manejan con una politica de remplazo lru
//debemos considerar que ya tenemos la politica de remplazo lru lista
//y que solo debemos crearla interaccion entre los niveles
//como funciona esta optimiacion?
//1.se revisa el nivel 1 si en este nivel es hit entonces OK
//2.si revisa el nivel 1 y es miss brinca l2
//3.se trabajan los parametros segun sea el caso de que escribe o solo lee
//4.se actuliazan los 2 niveles
int lru_replacement_policy_l1_l2(const entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug)
{
	//nuestra politica de remplazo sabemos que retorna Ok o ERROR
	//variables
	int i = 0; //contador
	bool estado = false; //miss
	
	//llamamos al cache nivel l1 para empezar o inicializarlo y trabajarlo desde adentro en la funcion original
	lru_replacement_policy(l1_l2_info->l1_idx,l1_l2_info->l1_tag,l1_l2_info->l1_assoc,loadstore,l1_cache_blocks,l1_result,debug);
	//recorremos las vias para limpiar dirty y dirty eviction
	for(i;i<l1_l2_info->l1_assoc;i++){
		//inicializamos los dirty y los dirty evictions en cero del cache nivel l1
		l1_cache_blocks[i].dirty = 0;
		l1_result->dirty_eviction = 0;
	}

	//recorremos las vias otra vez para comparar tags
	for(i;i<l1_l2_info->l1_assoc;i++){
		//ahora verificamos el tag para saber si es un hit o un miss en l1
		if(l1_cache_blocks[i].tag==l1_l2_info->l1_tag){
			//implica un hit  en el nivel l1
			//no pasa nada porque hay un hit en cache nivel l1
			estado = true; //bandera HIT
		}

	}
	//miss l1
	if(estado == false){ //bandera FALSE
		//tenemos miss en l1 
		//entonces llamamos al cache nivel l2 y se trabaja por dentro desde la funcion original
		lru_replacement_policy(l1_l2_info->l2_idx,l1_l2_info->l2_tag,l1_l2_info->l2_assoc,loadstore,l2_cache_blocks,l2_result,debug);
			
		}

	
return OK;
}