/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info  *l1_vc_info,
    	                     bool loadstore,
       	                   entry* l1_cache_blocks,
       	                   entry* vc_cache_blocks,
        	                 operation_result* l1_result,
              	           operation_result* vc_result,
                	         bool debug)
{
	//variables necesarias para el analisis
	bool find = 0;
	int pos_lib = 0;
	bool libre = 0;
	int val_max = 0;
	int lru = 0;
	int pos_dato = 0;
	bool libVC=false;
	int pos_libVC;
	bool hitVC=false;
	int pos_hitVC;
	bool candidatoVC=false;
	int pos_candidatoVC;
	//se obtiene la informacion del caso a analizar
	int tag= (int)l1_vc_info-> l1_tag;
	int idx= (int)l1_vc_info-> l1_idx;
	int associativity=(int)l1_vc_info-> l1_assoc;
	int vc_associativity=(int)l1_vc_info-> vc_assoc;
	for(int pos = 0; pos<associativity; pos++){
	   //se revisa si el bloque esta vacio
	   if(l1_cache_blocks[pos].valid == false){
	      pos_lib = pos; 
	      libre = true;
		}
		//se revisa si es un miss
		//se almacena el valor del LRU  
		if(l1_cache_blocks[pos].tag != tag){
			if(l1_cache_blocks[pos].rp_value > val_max){
        		val_max = l1_cache_blocks[pos].rp_value;
            	lru = pos;
        	} 
    	} 
		//se tiene un hit
		else{
		find=true;
        pos_dato = pos; 
        	if(loadstore){ 
            	l1_result->miss_hit = HIT_STORE;
            	vc_result->miss_hit = MISS_STORE;
            	l1_cache_blocks[pos_dato].dirty = true;
            	return OK;
        	} 
			else{
            	l1_result->miss_hit = HIT_LOAD;
            	vc_result->miss_hit = MISS_LOAD;
            	return OK;
        	}
        	for(int j = 0; j < associativity; j++){ //Actualizamos el LRU tomando en cuenta el hit
        		if(l1_cache_blocks[j].rp_value < l1_cache_blocks[pos].rp_value){
            		l1_cache_blocks[j].rp_value=l1_cache_blocks[j].rp_value+1;
          		}
        	}
         	l1_cache_blocks[pos].rp_value = 0;
      	}
   	}
	//no se encontro el dato en la cache L1
    if(!find){ 
		//se revisa si es una carga o una lectura
    	if(loadstore){
    		l1_result->miss_hit = MISS_STORE;
    	}
    	else{
        	l1_result->miss_hit = MISS_LOAD;
    	}
    	for(int pos = 0; pos<vc_associativity; pos++) {
        	//se revisa si la posicion esta libre, esto en vc 
			if(vc_cache_blocks[pos].valid==false){
        		if(libVC==false){ 
            		libVC=true;
            		pos_libVC=pos;
           		}
         	} 
        	else{
				//se revisa un miss
        		if(vc_cache_blocks[pos].tag!=tag){ 
				//se almacena la posicion de memoria
            		if(vc_cache_blocks[pos].rp_value==1 && !candidatoVC) { 
            			candidatoVC=true;
            			pos_candidatoVC=pos;
            		}
            	}
        		else{//hay un hit
					pos_hitVC=pos; 
            		hitVC=true;
        		}
        	}
   		}
		//CASE 1
		//no se tiene hit en L1 y tampoco en victim cache
		//se tiene una posicion libre en L1
    	if(libre && !hitVC){
			//se revisa si es una carga o una escritura 
        	if(loadstore){
        		vc_result->miss_hit = MISS_STORE;
        		l1_cache_blocks[pos_lib].dirty = true;
       		}
        	else{
        		vc_result->miss_hit = MISS_LOAD;
        	}
			//se actualizan los valores de la posicion libre
        	l1_cache_blocks[pos_lib].tag = tag;
        	l1_cache_blocks[pos_lib].valid = true;
			//se actualizan los valores de rp
        	for(int a = 0; a < associativity; a++){
            	if(l1_cache_blocks[a].valid){
            		l1_cache_blocks[a].rp_value=l1_cache_blocks[a].rp_value+1;
            	}
        	}
        	l1_cache_blocks[pos_lib].rp_value = 0;
    	}	  
		//CASE 2
		//no hay hit y L1 esta lleno, se tiene que proceder
		//a sustituir el LRU
		if(!libre && !hitVC){
			//se revisa si es una carga o una escritura, esto en vc 
        	if(loadstore){
        		vc_result->miss_hit = MISS_STORE;
        	}
        	else{
        		vc_result->miss_hit = MISS_LOAD;
        	}
        	//Se almacena el eviction a realizar
        	l1_result->dirty_eviction = l1_cache_blocks[lru].dirty;
        	l1_result->evicted_address = l1_cache_blocks[lru].tag;
        	l1_cache_blocks[lru].tag = tag;
			//se revisa si es una carga o una escritura  
        	if(loadstore){
        		l1_result->miss_hit = MISS_STORE;
        		l1_cache_blocks[lru].dirty = true;
        	}
        	else{
        		l1_result->miss_hit = MISS_LOAD;
        		l1_cache_blocks[lru].dirty = false;
        	}
			//se actualizan los valores de rp
        	for(int j = 0; j<associativity; j++){ 
        		l1_cache_blocks[j].rp_value=l1_cache_blocks[j].rp_value+1;
        	}
        	l1_cache_blocks[lru].rp_value = 0;
        	//Se procede a analizar el victim cache
        	//se analiza si hay una posicion libre en victim cache
			//si hay una posicion libre se almacena el valor en dicha posicion
			if(libVC){ 
        		vc_cache_blocks[pos_libVC].tag = l1_result->evicted_address; 
        		vc_cache_blocks[pos_libVC].valid = true;
        		vc_cache_blocks[pos_libVC].rp_value = 0;
        		vc_cache_blocks[pos_libVC].dirty = l1_result->dirty_eviction; 
        	}
			//si no hay espacio libre se debe proceder a sustituir
			//el cancadidato de VC
        	else{ 
				//Si no se tiene un candidato en VC se actualizan dichos rp
        		if(!candidatoVC){ 
            		for (int pos = 0; pos<vc_associativity; pos++) { //llena todas las vias de 1
            			vc_cache_blocks[pos].rp_value=1;
            		}
					//el candidato es el primer valor en este caso
            		pos_candidatoVC=0; 
        		}
				//se realiza el eviction en VC
        		vc_result->dirty_eviction = vc_cache_blocks[pos_candidatoVC].dirty;
        		vc_result->evicted_address = vc_cache_blocks[pos_candidatoVC].tag;
        		//se actualiza el vitcim cache con el ultimo evicted de L1
				vc_cache_blocks[pos_candidatoVC].tag = l1_result->evicted_address;
        		vc_cache_blocks[pos_candidatoVC].rp_value=0;
        		vc_cache_blocks[pos_candidatoVC].dirty = l1_result->dirty_eviction;
        	}
    	}
		//CASE 3
		//se tiene un hit  en VC
    	if(hitVC){ 
			//se actualiza el eviction
        	l1_result->dirty_eviction = l1_cache_blocks[lru].dirty;
        	l1_result->evicted_address = l1_cache_blocks[lru].tag;
        	//se revisa si es un load o un store
			if (loadstore){
        		l1_result->miss_hit = MISS_STORE;
        		l1_cache_blocks[lru].dirty = true; //Se actualiza el bit de dirty segun el loadstore
         	}
        	else{
        		l1_result->miss_hit = MISS_LOAD;
        		l1_cache_blocks[lru].dirty = false;
        	}
        	//Se procede a actualizar en L1 lo encontrado en victim cache
        	l1_cache_blocks[lru].tag = vc_cache_blocks[pos_hitVC].tag;
        	//se actualizan los rp
			for(int j = 0; j < associativity; j++){ 
        		l1_cache_blocks[j].rp_value=l1_cache_blocks[j].rp_value+1;
        	}
			//el nuevo bloque inicia en cero
        	l1_cache_blocks[lru].rp_value = 0;
        	vc_cache_blocks[pos_hitVC].tag = l1_result->evicted_address;
        	vc_cache_blocks[pos_hitVC].rp_value=0;
        	vc_cache_blocks[pos_hitVC].dirty=l1_result->dirty_eviction; 
        	if(loadstore){
           		vc_result->miss_hit = HIT_STORE;
        	}
        	else {
        		vc_result->miss_hit = HIT_LOAD;
        	}
    	}
       return OK;
	}
   return ERROR;
}
