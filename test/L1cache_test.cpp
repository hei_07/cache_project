/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>



class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {

  int status;
  int i;
  int j;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

  }

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {

  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

  }
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
//variables importantes
bool loadstoreA;
bool debug = false;
operation_result rA = {};
int tagA;
int tag;
int rpA;
//le ponemos valores aleatorios a el index
int idx = rand()%1024;
//paso 1. generar un rp random
int rp = rand()%3; //va de 0 a 2
//paso 2. escogemos una cantidad de vias random
int vias = 1 << (rand()%5); //tomamos vias de 1 a 4
//paso 3. llenemos la estructura entry del cache con las vias
struct entry cacheA[vias];
for(int i = 0; i < vias; i++){
  loadstoreA =false;
  if(rp == 0){ //LRU
      cacheA[i].valid = true;
      cacheA[i].tag = rand()%4096;
      cacheA[i].dirty = 0;
      cacheA[i].rp_value = (vias <= 2)? rand()%vias: 3;
  }
  else if(rp == 1){//NRU
    cacheA[i].valid = true;
    cacheA[i].tag = rand()%4096;
    cacheA[i].dirty = 0;
    cacheA[i].rp_value = i;

  }
  else if(rp == 2){//SRRIP
    cacheA[i].valid = true;
    cacheA[i].tag = rand()%4096;
    cacheA[i].dirty = 0;
    cacheA[i].rp_value = (vias <= 2)? rand()%vias: 3;
      
  }
}
//paso 4. insertamos un nuevo bloque A
  struct entry cacheAA[1];//de una via
  //inicializamos estructura
  if(rp == 0){ //LRU
      cacheAA[0].valid = true;
      cacheAA[0].tag = rand()%4096;
      cacheAA[0].dirty = 0;
      cacheAA[0].rp_value = (vias <= 2)? rand()%vias: 3;
  }
  else if(rp == 1){//NRU
    cacheAA[0].valid = true;
    cacheAA[0].tag = rand()%4096;
    cacheAA[0].dirty = 0;
    cacheAA[0].rp_value = 1;

  }
  else if(rp == 2){//SRRIP
    cacheAA[0].valid = true;
    cacheAA[0].tag = rand()%4096;
    cacheAA[0].dirty = 0;
    cacheAA[0].rp_value = (vias <= 2)? rand()%vias: 3;
  }
//paso5. forzamos un HIT en cacheAA

  tagA = tag;
  if(rp == 0){//lru
    lru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
  }
  else if(rp == 1){//nru
    nru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
  }
  else if(rp == 2){//srrip
    srrip_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
  }
//paso6. Revisamos el rp_value de mi cacheAA
  //se inicializan los rp por politica de remplazo ezperados
  int rpNRU = 0;
  int rpLRU = vias -1;
  int rpSRRIP = 0;
  rpA = cacheA[0].rp_value;
  if(rp == 0){//lru
    EXPECT_EQ(rpA,rpLRU);
  }
  else if(rp == 1){//nru
    EXPECT_EQ(rpA,rpNRU);
  }
  else if(rp == 2){//srrip
    EXPECT_EQ(rpA,rpSRRIP);
  }
//paso7. mantenerse insertando nuevos bloques hasta que A sea evicted
while(rA.evicted_address != cacheAA[0].tag){
    tag = rand()%4096;
    if(rp == 0){//lru
    lru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
    else if(rp == 1){//nru
      nru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
    else if(rp == 2){//srrip
      srrip_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
  }
  //paso8. revisamos el el bit de evicted de cacheAA antes de hacer evicted de otros bloques
    EXPECT_EQ(rA.evicted_address,cacheAA[0].tag);
 
}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
//variables importantes
bool loadstoreA;
bool loadstoreB;
bool debug = false;
operation_result rA = {};
operation_result rB = {};
int tagA;
int tagB;
int tag;
//le ponemos valores aleatorios a el index
int idx = rand()%1024;
//paso 1. generar un rp random
int rp = rand()%3; //va de 0 a 2
//paso 2. escogemos una cantidad de vias random
int vias = 1 << (rand()%5); //tomamos vias de 1 a 4
//paso 3. llenemos la estructura entry del cache con las vias
struct entry cacheA[vias];
for(int i = 0; i < vias; i++){
  loadstoreA =false;
  if(rp == 0){ //LRU
      cacheA[i].valid = true;
      cacheA[i].tag = rand()%4096;
      cacheA[i].dirty = 0;
      cacheA[i].rp_value = (vias <= 2)? rand()%vias: 3;
  }
  else if(rp == 1){//NRU
    cacheA[i].valid = true;
    cacheA[i].tag = rand()%4096;
    cacheA[i].dirty = 0;
    cacheA[i].rp_value = i;

  }
  else if(rp == 2){//SRRIP
    cacheA[i].valid = true;
    cacheA[i].tag = rand()%4096;
    cacheA[i].dirty = 0;
    cacheA[i].rp_value = (vias <= 2)? rand()%vias: 3;
      

  }
}
struct entry cacheB[vias];
for(int i = 0; i < vias; i++){
  loadstoreB =false;
  if(rp == 0){ //LRU
      cacheB[i].valid = true;
      cacheB[i].tag = rand()%4096;
      cacheB[i].dirty = 0;
      cacheB[i].rp_value = (vias <= 2)? rand()%vias: 3;
      

  }
  else if(rp == 1){//NRU
    cacheB[i].valid = true;
    cacheB[i].tag = rand()%4096;
    cacheB[i].dirty = 0;
    cacheB[i].rp_value = i;

  }
  else if(rp == 2){//SRRIP
    cacheB[i].valid = true;
    cacheB[i].tag = rand()%4096;
    cacheB[i].dirty = 0;
    cacheB[i].rp_value = (vias <= 2)? rand()%vias: 3;
  }
}
//paso4. forzamos un write hit en cacheA
  if(loadstoreA == 1){
    tagA = tag;
    if(rp == 0){//lru
      lru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
    else if(rp == 1){//nru
      nru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
    else if(rp == 2){//srrip
      srrip_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
  }//paso5. forzamos un read hit en cacheB
  loadstoreB = 0;
  tagB = tag;
  if(rp == 0){//lru
    lru_replacement_policy(idx,tag,vias,loadstoreB,cacheB,&rB,debug);
  }
  else if(rp == 1){//nru
    nru_replacement_policy(idx,tag,vias,loadstoreB,cacheB,&rB,debug);
  }
  else if(rp == 2){//srrip
    srrip_replacement_policy(idx,tag,vias,loadstoreB,cacheB,&rB,debug);
  }
  //paso6. forzamos un read hit en cacheA
  if(loadstoreA == 0){
    tagA = tag;
    if(rp == 0){//lru
      lru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
    else if(rp == 1){//nru
      nru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
    else if(rp == 2){//srrip
      srrip_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
  }
  //paso7. insetamos lineas hasta que B es evicted
  while(rB.evicted_address != tagB){
    tag = rand()%4096;
    if(rp == 0){//lru
    lru_replacement_policy(idx,tag,vias,loadstoreB,cacheB,&rB,debug);
    }
    else if(rp == 1){//nru
      nru_replacement_policy(idx,tag,vias,loadstoreB,cacheB,&rB,debug);
    }
    else if(rp == 2){//srrip
      srrip_replacement_policy(idx,tag,vias,loadstoreB,cacheB,&rB,debug);
    }
  //paso8. revisamos el el bit de dirty sea falso
    EXPECT_EQ(rB.dirty_eviction,false);
  }
  //paso9. insetamos lineas hasta que A es evicted
  while(rA.evicted_address != tagA){
    tag = rand()%4096;
    if(rp == 0){//lru
    lru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
    else if(rp == 1){//nru
      nru_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
    else if(rp == 2){//srrip
      srrip_replacement_policy(idx,tag,vias,loadstoreA,cacheA,&rA,debug);
    }
  //paso10. revisamos el el bit de dirty sea falso
    EXPECT_EQ(rA.dirty_eviction,false);
  }


}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
  //variables importantes
  bool loadstore;
  bool debug = false;
  int vias;
  operation_result rA = {};

  int tag;
  //le ponemos valores aleatorios a el index
  int idx = rand()%1024;
  //paso 1. generar un rp random
  int rp = rand()%3; //va de 0 a 2
  //paso2. le ponemos parametros invalidos al idx, tag y associtividad
  tag = -2;
  idx = -6;
  vias = 4;
  //llenamos un cache
  struct entry cache[vias];
  for(int i = 0; i < vias; i++){
    loadstore =false;
    if(rp == 0){ //LRU
      cache[i].valid = true;
      cache[i].tag = rand()%4096;
      cache[i].dirty = 0;
      cache[i].rp_value = (vias <= 2)? rand()%vias: 3;
    }
     else if(rp == 1){//NRU
      cache[i].valid = true;
      cache[i].tag = rand()%4096;
      cache[i].dirty = 0;
      cache[i].rp_value = i;

    }
    else if(rp == 2){//SRRIP
      cache[i].valid = true;
      cache[i].tag = rand()%4096;
      cache[i].dirty = 0;
      cache[i].rp_value = (vias <= 2)? rand()%vias: 3;
    }
  }
  
  bool param = PARAM;
  bool paramR = true;
  //paso3. revisamos si la funcion nos retorna un param error
  if(rp == 0){//lru
    lru_replacement_policy(idx,tag,vias,loadstore,cache,&rA,debug);
  }
  else if(rp == 1){//nru
    nru_replacement_policy(idx,tag,vias,loadstore,cache,&rA,debug);
  }
  else if(rp == 2){//srrip
    srrip_replacement_policy(idx,tag,vias,loadstore,cache,&rA,debug);
  }
  EXPECT_EQ(param,paramR);

}
